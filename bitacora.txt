SEFORA ARCOS MONTOYA
------------------------------

1.-CREACION DE DIRECTORIO Departamento
2.-Creando archivo bitacora.txt
3.-Iniciando repositorio
- git init
4.-añadiendo al area de stage al archivo bitacora.txt
- git add bitacora.txt
5.-modificando archivo bitacora.txt
6.-añadiendo al area de stage el archivo bitacora.txt previa verificacion
- git add -p bitacora.txt
7.- haciendo commit de archivo bitacora.txt previa verificacion
- git commit -pm "cambios realizados a archivo bitacora desde paso 1 al 6"
8.-Creando y agregando contenido a  fichero platos-tipicos.txt 
- git add -A
- git commit -am "Se creo fichero platos_tipicos.txt se hizo cambios a bitacora.txt"
9.-Vista Detallada de los cambios realizados en un determinado commit
- git show 717b32653d87929179b1e1f02a30543a54e2071f
10.- Revirtiendo los cambios realizados en determinado commit
- git revert 717b32653d87929179b1e1f02a30543a54e2071f
11.- Verificando nuevos cambios
- git status
12.- Agregando al area de stage archivo modificado 
-  git add bitacora.txt
13-. Realizando commit sobre el archivo modificado
- git commit -m "agregando punto del 7-13 a archivo bitacora."
14.- volviendo a Creando y agrega a stage archivo platos_tipicos.txt con su contenido.
- git add platos_tipicos.txt
15.- Realizando commit de los cambios sobre archivos platos_tipico.txt y bitacora.txt
- git commit -am "cambios sobre archivo bitacora, creacion y cambios realizados en archivo platos_tipicos.txt"

16.- Creando y agregando cotenido a fichero fesstividad.txt, agregando fichero a area de stage
- git add .
17.- agregadno modificaciones realizadas en una pila
- git stash
18.- Agregando cambios de pila a directorio de trabajo
- git stash apply stash@{0}
19.- llevando a stage cambios realizados a bitacora.txt, y realizando commit de cambios en bitacora.txt y cambios en festividad.txt recuperadas de pila.
- git add bitacora.txt
- git commit -am "Cambios relizados a bitacora.txt y festividad.txt" 

